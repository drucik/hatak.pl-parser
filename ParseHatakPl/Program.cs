﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using HtmlAgilityPack;
using OfficeOpenXml;

namespace ParseHatakPl
{
    class Program
    {
        private static Dictionary<DateTime, List<SerialEpisode>> days = new Dictionary<DateTime, List<SerialEpisode>>();
        private static IEnumerable<string> serialNamesBlackList;
        private static IEnumerable<string> serialNamesSelectedList;

        static void Main(string[] args)
        {
            serialNamesBlackList = GetSerialNamesOnBlackList();
            serialNamesSelectedList = GetSerialNamesOnSelectedList();

            DateTime date = DateTime.Now.Date;
            DateTime endDate = date.AddMonths(1);
            while (date < endDate)
            {
                GetEpisodesFor(date, serialName => serialNamesBlackList.Contains(serialName), serialName => serialNamesSelectedList.Contains(serialName));
                date = date.AddDays(1);
            }
            UpdateSerialNames();
            WriteToExcel();
        }
  
        private static void UpdateSerialNames()
        {
            List<string> serialNames = new List<string>();
            if (File.Exists("serialnames.txt") == true)
            {
                string[] lines = File.ReadAllLines("serialnames.txt");
                serialNames.AddRange(lines.Where(line => string.IsNullOrWhiteSpace(line.Trim()) == false).Select(line => line.Trim()));
            }
            serialNames.AddRange(days.Values.SelectMany(l => l.Select(s => s.Serial)).Distinct().Except(serialNames));
            File.WriteAllLines("serialnames.txt", serialNames);
        }

        private static IEnumerable<string> GetSerialNamesOnBlackList()
        {
            if (File.Exists("serials-blacklist.txt") == false)
                return new List<string>();

            string[] lines = File.ReadAllLines("serials-blacklist.txt");
            return lines.Where(line => string.IsNullOrWhiteSpace(line.Trim()) == false).Distinct().Select(line => line.Trim());
        }
        
        private static IEnumerable<string> GetSerialNamesOnSelectedList()
        {
            if (File.Exists("serials-selected.txt") == false)
                return new List<string>();

            string[] lines = File.ReadAllLines("serials-selected.txt");
            return lines.Where(line => string.IsNullOrWhiteSpace(line) == false).Distinct();
        }
  
        private static void GetEpisodesFor(DateTime date, Func<string, bool> isOnBlackList, Func<string, bool> isOnSelectedList)
        {
            List<SerialEpisode> episodes;
            if (days.TryGetValue(date, out episodes) == false)
            {
                episodes = new List<SerialEpisode>();
                days.Add(date, episodes);
            }

            using (WebClient client = new WebClient())
            {
                client.Encoding = System.Text.Encoding.UTF8;
                string data = client.DownloadString(string.Format("http://hatak.pl/kalendarz?day={0:yyyy-MM-dd}&filter=all", date));
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(data);

                var elements = doc.DocumentNode.SelectNodes("//*[@id='content']/div[@class='calendar-entry']/*/div[@class='three columns']");
                if (elements == null)
                    return;
                foreach (HtmlNode htmlElement in elements)
                {
                    string serialName = htmlElement.ChildNodes.SingleOrDefault(n => n.NodeType == HtmlNodeType.Element && n.Name == "a").InnerHtml.Trim();
                    if (isOnBlackList(serialName))
                        continue;

                    HtmlNode episodeNumberNode = htmlElement.ChildNodes.SingleOrDefault(n => n.NodeType == HtmlNodeType.Element && n.Name == "span");
                    string episodeNumber = episodeNumberNode.ChildNodes[0].ChildNodes[0].InnerText;

                    episodes.Add(new SerialEpisode(episodeNumber, serialName, isOnSelectedList(serialName)));
                }
            }
        }

        private static int lastRowIndex = 1;
        private static readonly Dictionary<string, int> mappedSerialToRowIndex = new Dictionary<string, int>();
        public static void WriteToExcel()
        {
            var fileInfo = new FileInfo("output.xlsx");
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
            using (ExcelPackage excel = new ExcelPackage(fileInfo))
            {
                ExcelWorksheet worksheet = excel.Workbook.Worksheets.SingleOrDefault(w => w.Name == "Serials");
                if (worksheet != null)
                    excel.Workbook.Worksheets.Delete(worksheet);
                worksheet = excel.Workbook.Worksheets.Add("Serials");

                MapSerialsToRows(worksheet);

                int columnIndex = 1;
                foreach (DateTime date in days.Keys)
                {
                    worksheet.Cells[1, ++columnIndex].Value = date.ToString("dd-MM-yyyy");
                    List<SerialEpisode> episodes = days[date];
                    foreach (SerialEpisode episode in episodes)
                    {
                        int rowIndex = mappedSerialToRowIndex[episode.Serial];
                        worksheet.Cells[rowIndex, columnIndex].Value = episode.EpisodeNumber;
                    }
                }
                for (int i = 1; i <= columnIndex; ++i)
                    worksheet.Column(i).AutoFit();
                excel.Save();
            }
        }
  
        private static void MapSerialsToRows(ExcelWorksheet worksheet)
        {
            int lastRowIndex = 1;

            foreach (var serial in days.Values.SelectMany(l => l.Select(s => s)).OrderByDescending(s => s.IsSelected).Select(s => new { Name = s.Serial, IsSelected = s.IsSelected }))
            {
                if (mappedSerialToRowIndex.ContainsKey(serial.Name))
                    continue;

                int rowIndex = ++lastRowIndex;
                mappedSerialToRowIndex.Add(serial.Name, rowIndex);
                worksheet.Cells[rowIndex, 1].Value = serial.Name;
                if (serial.IsSelected)
                {
                    worksheet.Cells[rowIndex, 1].Style.Font.Bold = true;
                    worksheet.Cells[rowIndex, 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells[rowIndex, 1].Style.Font.Color.SetColor(Color.FromArgb(0xFF, 0x00, 0x61, 0x00));
                    worksheet.Cells[rowIndex, 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0xFF, 0xC6, 0xEF, 0xCE));
                }
            }
        }
    }

    public class SerialEpisode
    {
        public SerialEpisode(string episodeNumber, string serial, bool isSelected)
        {
            EpisodeNumber = episodeNumber;
            Serial = serial;
            IsSelected = isSelected;
        }

        public string Serial { get; set; }
        public string EpisodeNumber { get; set; }
        public bool IsSelected { get; set; }
    }
}
